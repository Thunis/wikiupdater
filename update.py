#!/usr/bin/env python3

import os
import os.path
import shutil
from sys import stderr, exit
import subprocess

# CONFIG

keep_extensions = ["HtmlFromFile", "ImportUsers"]

# CONFIG END

assert __name__ == "__main__"

OLDW = "w"
NEWW = "w.new"

MUST_OVERWRITE = set(["images"])

class Move:
	def __init__(self, path_in_wiki):
		self._piw = path_in_wiki
	def get_src_path(self):
		return os.path.join(OLDW,self._piw)
	def get_dst_path(self):
		return os.path.join(NEWW,self._piw)
	def must_overwrite(self):
		return self._piw in MUST_OVERWRITE
	def src_valid(self):
		src_exists = os.path.exists(self.get_src_path())
		if not src_exists:
			print("File is to be copied but doesn't exist:", self._piw, file=stderr)
		return src_exists
	def dst_valid(self):
		p = self.get_dst_path()
		would_overwrite = os.path.exists(p)
		if would_overwrite and not self.must_overwrite():
			print("File already exists in new wiki:", self._piw, file=stderr)
		elif not would_overwrite and self.must_overwrite():
			print("File should already exist in new wiki but doesn't:", self._piw, file=stderr)
		pp = os.path.dirname(p)
		parent_exists = os.path.isdir(pp)
		if not parent_exists:
			print("File would have no parent in the new wiki:", self._piw, file=stderr)
		return parent_exists and (would_overwrite == self.must_overwrite())
	def valid(self):
		return self.src_valid() and self.dst_valid()
	def __str__(self):
		return self.get_src_path()+" -> "+self.get_dst_path()
	def execute(self):
		assert self.valid()
		if self.must_overwrite() and os.path.isdir(self.get_dst_path()):
			shutil.rmtree(self.get_dst_path())
		os.renames(self.get_src_path(), self.get_dst_path())

def check_moves(moves):
	# we can't use all() because we also want warnings not only for the first problem
	all_valid = True
	for move in moves:
		valid = move.valid()
		if not valid:
			all_valid = False
	return all_valid


assert os.path.isdir("w")
assert os.path.isdir("w.new")
assert not os.path.exists("w.old")

files_to_move = ["LocalSettings.php", "images"] + list(map(lambda x: os.path.join("extensions",x), keep_extensions))
moves = list(map(Move, files_to_move))

all_valid = check_moves(moves)
if not all_valid:
	exit(1)

print("MOVES:")
for move in moves:
	print(move)
print("ALL FILE MOVES SEEM TO BE VALID")
begin = input("Execute update? (y/n)")
if begin != "y":
	exit("ABORTED BY USER")

print("Executing moves")
for move in moves:
	move.execute()

print("Swapping old and new wiki")

os.rename("w", "w.old")
os.rename("w.new", "w")

print("STARTING DATABASE UPDATE")

proc = subprocess.Popen(["php","update.php","--quick"], cwd="w/maintenance")

db_update_ret_code = proc.wait()

if db_update_ret_code != 0:
	exit("DATABASE UPGRADE SEEMS TO HAVE FAILED!")

print("Database upgrade seems to have worked.")

print("Please take a moment to check if your installed extensions are still up to date:")
print(keep_extensions)

